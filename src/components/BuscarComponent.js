import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, TextInput } from 'react-native'
import SuperheroeService from '../services/SuperheroeService';
import SuperheroeCard from './SuperheroeCard'
import { Icon } from 'react-native-elements';

const styles = StyleSheet.create({

    container:{
        padding: 20
    },
    row:{
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    boton:{
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'

    },
    botonBuscar: {
        backgroundColor: "#ffffff",
        borderColor: "#27ae60",   
        borderWidth: 1,
        borderTopLeftRadius:0,
        borderBottomLeftRadius:0,
        alignContent: "center",
        width: "20%"  
    },
    textBuscar: {
        color: "#27ae60"
    },
    input:{
        padding: 10,
    },
    inputBuscar: { 
        borderColor: 'gray', 
        borderWidth: 1 ,
        width: "80%"
        
    }
    

})



export default class BuscarComponent extends Component {
    constructor(props){
        super(props);
    

        this.state = {
            nombre: ""
        }
    }

    render() {
        
        const {nombre} = this.state
        const {nombreBuscado} = this.props
        
        return (
            <>
                <View style={styles.container}>
                    <Text>Buscar</Text>
                    <View style={styles.row}>
                        <TextInput  
                            style={[styles.input, styles.inputBuscar]}
                            placeholder="Ingresa el nombre del superhéroe"
                            value={nombre}
                            onChangeText={(value) =>this.setState({nombre: value})}
                        ></TextInput>
                        <TouchableOpacity style={[styles.boton, styles.botonBuscar]} onPress={() => nombreBuscado(nombre)}>
                            <Icon
                                name='search'
                                type='font-awesome'
                                size={20}
                                color="#27ae60"
                            ></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}
