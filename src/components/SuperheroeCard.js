import React, { Component } from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#bdc3c7",
        borderRadius: 7,
        marginBottom: 10,
    },
    imageContainer:{
        height:200,
    },
    image:{
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
        backgroundColor: "black",
        width: "100%",
        height: "100%"
    },
    textContainer:{
        padding: 10
    },
    title: {
        fontWeight: "bold",
        fontSize: 15,
        textAlign: 'center'
    },
    information:{
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10,
        alignItems: "center"
    },
    link:{
        padding: 10,
        color: "#3498db",
        textDecorationLine: "underline" 
    },
    buttonContainer:{
        backgroundColor: "#27ae60",
        borderBottomLeftRadius: 7,
        borderBottomRightRadius: 7,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
        color:"#ecf0f1",
        fontSize: 20,
    } 
})



const RenderBoton = ({idSuperheroe, navega}) => {

    return (
        <TouchableOpacity style={styles.buttonContainer} onPress={() => navega(idSuperheroe)}>
            <Text style={styles.buttonText}>
                Ver detalles
            </Text>
        </TouchableOpacity>
    )
    
}

export default class SuperheroeCard extends Component {
    

    render() {

        const {id, name, image, navega} = this.props
        return (
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <Text style={styles.title}>
                        {name}
                    </Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image style={styles.image}
                        resizeMode="cover" 
                        source={{uri:image}}/>
                </View>
                <RenderBoton idSuperheroe={id} navega={navega}></RenderBoton>
            </View>
        )
    }
}
