import React, { Component, useEffect, useState} from 'react'
import { FlatList, StyleSheet, View, Text} from 'react-native'
import SuperheroeService from '../services/SuperheroeService'
import SuperheroeCard from './SuperheroeCard'
import TituloComponent from './TituloComponent'
import Spinner from 'react-native-loading-spinner-overlay';
import Util from '../utils/Util'


const styles = StyleSheet.create({
    flatListContainer:{
        padding: 20,
        width: '100%',
        height: '100%',
        marginBottom: 10

    },
    spinnerTextStyle: {
        color: '#FFF'
    },

})

const ListaSuperheroesComponent = ({navega, nombreBuscar}) => {
    
    const [superheroes, setSuperheroes] = useState([])
    const [visibleSpinner, setVisibleSpinner] = useState(false)
    const [nombre, setNombre] = useState("")

    useEffect(() => {
        if(nombre != ""){
            setVisibleSpinner(true)
            obtenerSuperheroes()
        }
    }, [nombre])


    useEffect(() => {
        setNombre(nombreBuscar)
    }, [nombreBuscar])


    const obtenerSuperheroes = () =>{
        SuperheroeService.findSuperheroByName(nombre)
        .then(resp => {
            const data = resp.data.results
            
            const listaSuperheroes = []
            data.map(({id, name, image:{url}}) => {
                listaSuperheroes.push({"id": id, "name":name, "image":url})
            })
            setSuperheroes(listaSuperheroes)
            setVisibleSpinner(false)
        })
        .catch(err => {
            setSuperheroes([])
            setVisibleSpinner(false)

        })

    }


    const generarListadoAleatorio = () => {
        setNombre(Util.obtenerNombreAleatorio())
    }
    
    const navegar = (idSuperheroe) => {
        navega(idSuperheroe)
    }

    
        return (
            <>
                <Spinner 
                    visible={visibleSpinner}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <TituloComponent titulo="Lista aleatoria de superhéroes" botonVisible={true} funcionBoton={generarListadoAleatorio}/>
                <FlatList
                    data={superheroes}
                    style={styles.flatListContainer}
                    keyExtractor={({id}) => id.toString()}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item:{id, name, image}}) => {
                        return(
                            <SuperheroeCard id={id} name={name} image={image} navega={navegar}></SuperheroeCard>
                        )
                    }}
                    ListEmptyComponent={() => (
                        <View>
                            <Text>
                                No hay Superheroes
                            </Text>
                        </View>
                    )}
                >
                
                </FlatList>
            </>
        )
    
}


export default ListaSuperheroesComponent 
