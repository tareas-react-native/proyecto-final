import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Icon } from 'react-native-elements';


const styles = StyleSheet.create({
    container:{
        padding: 20,
    },
    tituloContainer:{ 
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    horizontalLine:{
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: 20
    },
    boton:{
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#ffffff",
        alignContent: "center",
    },
    textBoton: {
        color: "#27ae60"
    },
})

const RenderBoton = ({val, func}) => {
    if (val){
        return  <TouchableOpacity style={styles.boton} onPress={() => func()}>
                    <Icon
                        name='retweet'
                        type='ant-design'
                        size={20}
                        color="#27ae60"
                      ></Icon>
                </TouchableOpacity>
    }
    return null
}
export default class TituloComponent extends Component {
    
    
        
    render() {
        const {titulo, botonVisible, funcionBoton} = this.props
        
        return (
            <View style={styles.container}>
                <View style={styles.tituloContainer}>
                    <Text>
                        {titulo}
                    </Text>
                    <RenderBoton val={botonVisible} func={funcionBoton}></RenderBoton>
                </View>
                
                <View style={styles.horizontalLine}/>
            </View>
            
        )
    }
}
