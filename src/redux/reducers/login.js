const defaultState = {
    valid: undefined,
    sessionId: null
}

const login = (state = defaultState, action) => {
    switch (action.type) {
        case 'START_LOADING':
            return {
                ... state,
                loading: true
            }
        case 'END_LOADING':
            return {
                ... state,
                loading: false
            }
        case 'LOGIN_SUCCESSFULL':
            return {
                ... state,
                valid: action.valid
            }
        default:
            return state
    }
}

export default login;