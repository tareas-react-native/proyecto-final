
export const login = ({user, pass}) => {

    return async (dispatch) => {
        dispatch({
            type: 'START_LOADING',
        })

        await new Promise(resolve => setTimeout(() => resolve(), 2000))

        dispatch({
            type: 'END_LOADING',
        })

        if(user === 'admin' && pass === 'super'){
            dispatch({
                type: 'LOGIN_SUCCESSFULL',
                valid: true
            })
        }


    }
} 

export const logout = () => {

    return async (dispatch) => {
        dispatch({
            type: 'START_LOADING',
        })

        dispatch({
            type: 'LOGIN_SUCCESSFULL',
            valid: false
        })
    
        await new Promise(resolve => setTimeout(() => resolve(), 5000))

        dispatch({
            type: 'END_LOADING',
        })
    }}
