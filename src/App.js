import React from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import store from './redux/store';
import RootNavigation from './routes/RootNavigation';

const App: () => React$Node = () => {
	return (
		<>
			<StatusBar barStyle="dark-content" />
			<Provider store={store}>
				<RootNavigation />
			</Provider>

		</>
	);
};

export default App;
