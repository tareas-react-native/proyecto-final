import apiClient from '../services/axios'


const findSuperheroById = async (id) => {
    try {
        return await apiClient.get(`${id}/image`) 
    } catch (err) {
        throw new Error('Error al traer los superheroes.')
    }
};


const findBiographyById = async (id) => {
    try {
        return await apiClient.get(`${id}/biography`) 
    } catch (err) {
        throw new Error('Error al traer la biografia.')
    }
};


const findAppearanceById = async (id) => {
    try {
        return await apiClient.get(`${id}/appearance`) 
    } catch (err) {
        throw new Error('Error al traer la apariencia.')
    }
};



const findWorkById = async (id) => {
    try {
        return await apiClient.get(`${id}/work`) 
    } catch (err) {
        throw new Error('Error al traer el trabajo.')
    }
};



const findPowerstatsById = async (id) => {
    try {
        return await apiClient.get(`${id}/powerstats`) 
    } catch (err) {
        throw new Error('Error al traer nivel de poder.')
    }
};


const findConnectionsById = async (id) => {
    try {
        return await apiClient.get(`${id}/connections`) 
    } catch (err) {
        throw new Error('Error al traer nivel de poder.')
    }
};


const findSuperheroByName = async (name) => {
    try {
        return await apiClient.get(`/search/${name}`) 
    } catch (err) {
        throw new Error(`Error al traer los superheroes por nombre.${err}`)
    }
};


export default {
    findSuperheroById,
    findBiographyById,
    findAppearanceById,
    findWorkById,
    findPowerstatsById,
    findConnectionsById,
    findSuperheroByName
};