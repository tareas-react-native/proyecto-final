import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import Home from '../screens/Home';
import SuperHeroeCard from '../screens/SuperHeroeCard';

const ScreenStack = createStackNavigator()

const HomeScreenStack = () =>{
    return (
        <ScreenStack.Navigator initialRouteName="Home" >
            <ScreenStack.Screen name="Home" options={{title:"Home"}} component={Home}/>
            <ScreenStack.Screen name="SuperHeroeCard" options={{title:"Detalle del Superheroe"}} component={SuperHeroeCard}/>
        </ScreenStack.Navigator>
    )
}


export default HomeScreenStack;