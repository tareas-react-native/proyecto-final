import React from 'react';
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import BottomTabsStack from './BottomTabsStack';
import Login from '../screens/Login';
import { connect } from 'react-redux'

const RootStack = createStackNavigator()

const RootNavigation = ({isLoginValid}) => {
    return (
        <NavigationContainer>
            <RootStack.Navigator headerMode='none'>
                {
                    isLoginValid ? (
                        <RootStack.Screen name="Tabs" component={BottomTabsStack}/>
                    ) : (
                        <RootStack.Screen name="Login" component={Login}/>
                    )
                }
            </RootStack.Navigator>
        </NavigationContainer>
    )

}


// PARA LOS ESTADOS
const mapStateToProps = (state) => ({
    isLoginValid: state.login.valid,
})


    
export default connect(mapStateToProps)(RootNavigation);
