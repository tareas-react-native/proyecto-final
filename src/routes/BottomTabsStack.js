import { BottomTabBar, BottomTabView, createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Home from "../screens/Home"
import Perfil from '../screens/Perfil'
import HomeScreenStack from './HomeScreenStack'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Logout from '../screens/Logout'


const styles = StyleSheet.create({
    icon: {
        flexDirection: 'row',
        justifyContent:'center',
        alignItems: 'center'
    }
})

const BottomNav = createBottomTabNavigator()

const BottomTabsStack = () => {

    return (
        <BottomNav.Navigator tabBarOptions={{showLabel: false}} >
            <BottomNav.Screen 
                name="Home" 
                component={HomeScreenStack} 
                options={{
                    tabBarIcon: ({color}) => {
                        MaterialCommunityIcon.loadFont()
                        return (
                            <View style={styles.icon}>
                                <MaterialCommunityIcon name="home-circle" color={color} size={30}/>
                                <Text style={{color, marginLeft: 10}}>Home</Text>
                            </View>
                        )
                    }
                }}
            />
            <BottomNav.Screen 
                name="Perfil" 
                component={Perfil} 
                options={{
                    tabBarIcon: ({color}) => {
                        MaterialCommunityIcon.loadFont()
                        return (
                            <View style={styles.icon}>
                                <MaterialCommunityIcon name="account-circle" color={color} size={30}/>
                                <Text style={{color, marginLeft: 10}}>Perfil</Text>
                            </View>
                        )
                    }
                }}
            />
            <BottomNav.Screen 
                name="Salir" 
                component={Logout} 
                options={{
                    tabBarIcon: ({color}) => {
                        MaterialCommunityIcon.loadFont()
                        return (
                            <View style={styles.icon}>
                                <MaterialCommunityIcon name="logout" color={color} size={30}/>
                                <Text style={{color, marginLeft: 10}}>Salir</Text>
                            </View>
                        )
                    }
                }}
            />
        </BottomNav.Navigator>
    )
}

export default BottomTabsStack 
