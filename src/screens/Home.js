import React, { Component, useEffect, useState } from 'react'
import BuscarComponent from '../components/BuscarComponent'
import ListaAleatoriaComponent from '../components/ListaSuperheroesComponent'


const Home = ({navigation}) => {

    const [nombre, setNombre] = useState("")

    const navegar = (id) => {
        navigation.navigate("SuperHeroeCard",{idSuperheroe: id})
    }

    
    return (
        <>
            <BuscarComponent nombreBuscado={setNombre}></BuscarComponent>
            <ListaAleatoriaComponent navega={navegar} nombreBuscar={nombre} ></ListaAleatoriaComponent>
        </>
    )
}

export default Home
