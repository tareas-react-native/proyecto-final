import React, { useEffect, useState } from 'react'
import { FlatList, Image, Modal, StyleSheet, Text, TouchableOpacity, View, Button, Alert } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import TituloComponent from '../components/TituloComponent'
import SuperheroeService from '../services/SuperheroeService'
import Util from '../utils/Util'


const styles = StyleSheet.create({
    
    flatListContainer:{
        padding: 20,
        width: '100%',
        height: '100%',
        marginBottom: 10

    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    imageContainer:{
        height:"50%",
        alignItems: 'center',
        marginBottom: 20
    },
    image:{
        borderRadius: 10,
        backgroundColor: "black",
        width: "50%",
        height: "100%"
    },
    buttonContainer:{
        backgroundColor: "#27ae60",
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    buttonText:{
        color:"#ecf0f1",
        fontSize: 20,
    },
    modal: {  
        alignItems: 'center',   
        backgroundColor : "#fff",   
        height: "90%" ,  
        width: '95%',  
        borderRadius:10,  
        borderWidth: 1,  
        borderColor: '#fff',    
        margin: 10,   
    },
    closeText: {
        backgroundColor: "#e74c3c",
        color: "#ecf0f1",
        borderRadius: 15,
        width: 32,
        padding: 6,
        alignSelf: 'flex-end',
        textAlign: 'center',
        borderWidth: 1,
        borderColor: "#ecf0f1",
    }

})

const SuperHeroeCard = ({route}) => {
    
    const [idSuperhero, setIdSuperhero] = useState(route.params.idSuperheroe)
    //const [foto, setFoto] = useState("https://cdn.icon-icons.com/icons2/1465/PNG/512/277bustinsilhouette_100921.png")
    const [foto, setFoto] = useState(null)
    const [nombre, setNombre] = useState("")
    const [visibleSpinner, setVisibleSpinner] = useState(false)
    const [visibleModal, setVisibleModal] = useState(false)
    const [modalData, setModalData] = useState(null)

    useEffect(() => {
        setVisibleSpinner(true)
        SuperheroeService.findSuperheroById(idSuperhero)
        .then(resp => {
            setFoto(resp.data.url)
            setNombre(resp.data.name)
                
            setVisibleSpinner(false)
        })
        .catch(err => {
            console.log("ERROR", err)
            setVisibleSpinner(false)
        })
    }, [])

    const abrirModal = (idServicio) => {
        setVisibleSpinner(true)
        switch(idServicio) {
 
            case '1':
                SuperheroeService.findPowerstatsById(idSuperhero)
                .then(resp => {
                    const {combat, durability, intelligence, power, speed, strength} = resp.data

                    setModalData(
                        <>
                            <Text>Combate</Text>
                            <Text>{combat}</Text>
                            <Text>Surabilidad</Text>
                            <Text>{durability}</Text>
                            <Text>Inteligencia</Text>
                            <Text>{intelligence}</Text>
                            <Text>Poder</Text>
                            <Text>{power}</Text>
                            <Text>Velocidad</Text>
                            <Text>{speed}</Text>
                            <Text>Fuerza</Text>
                            <Text>{strength}</Text>
                        </>
                    )

                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)

                })
                break;
            
            case '2':
                SuperheroeService.findBiographyById(idSuperhero)
                .then(resp => {
                    //const {aliases, alignment, alter-egos, first-appearance, full-name, place-of-birth, publisher} = resp.data
                    const {data} = resp
                    
                    setModalData(
                        <>
                            <Text>Alias</Text>
                            <Text>{data["aliases"]}</Text>
                            <Text>Bando</Text>
                            <Text>{data["alignment"]}</Text>
                            <Text>Alter Egos</Text>
                            <Text>{data["alter-egos"]}</Text>
                            <Text>Primera Aparición</Text>
                            <Text>{data["first-appearance"]}</Text>
                            <Text>Nombre Completo</Text>
                            <Text>{data["full-name"]}</Text>
                            <Text>Lugar de Nacimiento</Text>
                            <Text>{data["place-of-birth"]}</Text>
                            <Text>Edición</Text>
                            <Text>{data["publisher"]}</Text>
                        </>
                    )
                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                })
                break;       
            case '3':
                SuperheroeService.findAppearanceById(idSuperhero)
                .then(resp => {
                    const {data} = resp
                    
                    setModalData(
                        <>
                            <Text>Genero</Text>
                            <Text>{data["gender"]}</Text>
                            <Text>Color de Ojos</Text>
                            <Text>{data["eye-color"]}</Text>
                            <Text>Color de Cabello</Text>
                            <Text>{data["hair-color"]}</Text>
                            <Text>Altura</Text>
                            <Text>{data["height"]}</Text>
                            <Text>Peso</Text>
                            <Text>{data["weight"]}</Text>
                            <Text>Raza</Text>
                            <Text>{data["race"]}</Text>
                        </>
                    )
                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                }) 
                break;       
      
            case '4':
                SuperheroeService.findWorkById(idSuperhero)
                .then(resp => {
                    const {data} = resp
                    
                    setModalData(
                        <>
                            <Text>Base</Text>
                            <Text>{data["base"]}</Text>
                            <Text>Empleo</Text>
                            <Text>{data["occupation"]}</Text>
                        </>
                    )
                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                }) 
                break;  

            case '5':
                SuperheroeService.findConnectionsById(idSuperhero)
                .then(resp => {
                    const {data} = resp
                    
                    setModalData(
                        <>
                            <Text>Grupo(s)</Text>
                            <Text>{data["group-affiliation"]}</Text>
                            <Text>Parientes y amigos</Text>
                            <Text>{data["relatives"]}</Text>
                        </>
                    )
                    setVisibleModal(true)
                    setVisibleSpinner(false)

                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                }) 
                break;  

            default:
                Alert.alert("mensaje", "Opción no válida")
            }    
    }

    return (
        <>
            <Spinner 
                visible={visibleSpinner}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            
                        
            <TituloComponent titulo={nombre}></TituloComponent>
            <View style={styles.imageContainer}>
                <Image style={styles.image}
                    resizeMode="cover"
                    source={{ uri: foto }} 
                    />
            </View>
            <FlatList
                data={Util.obtenerMenuDetalleSuperheroes()}
                style={styles.flatListContainer}
                keyExtractor={({id}) => id.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={({item:{id, nombre}}) => {
                    return(
                        <TouchableOpacity key={id} style={styles.buttonContainer} onPress={() => abrirModal(id)}>
                            <Text style={styles.buttonText}>
                                {nombre}
                            </Text>
                        </TouchableOpacity>
                    )
                }}
            >
                
            </FlatList>
            <Modal            
                animationType = {"fade"}  
                transparent = {false}  
                visible = {visibleModal}  
                onRequestClose = {() => setVisibleModal(false)}
            >  

                <View >
                    <TouchableOpacity
                        onPress={() => setVisibleModal(false)}
                    >
                        <Text style={styles.closeText}>X</Text>
                    </TouchableOpacity>
                    <View style = {styles.modal}>  
                        {modalData}
                    </View> 
                </View>  
            </Modal>  

        </>
    )

}
export default SuperHeroeCard