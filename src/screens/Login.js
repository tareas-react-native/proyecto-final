import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import { login } from '../redux/actions'
import Spinner from 'react-native-loading-spinner-overlay';

const styles = StyleSheet.create({
    header: {
        backgroundColor: "#2c3e50",
        height: 350,
        justifyContent: 'flex-end'
    },
    login:{
        margin: 20,
        fontSize: 40,
        color: '#ecf0f1'
    },
    inputContainer: {
        paddingTop: 50,
        flexDirection: 'row',
        justifyContent :'flex-start',
        alignItems: 'center',
        backgroundColor: '#ecf0f1'

    },
    textinputContainer:{
        width: '80%',
        paddingVertical: 20,
        paddingRight : 20,
        borderColor: '#16a085',
        borderWidth: 1,
        borderTopRightRadius: 100,
        borderBottomRightRadius: 100,
        justifyContent: 'space-between'
    },
    textInput:{
        fontSize: 20,
        marginVertical: 10,
        paddingLeft: 10,
        color: '#95a5a6'
    },
    inputSubmit: {
         backgroundColor: '#16a085',
         padding: 5,
         right:40,
         borderRadius: 35

    },
    spinnerTextStyle: {
        color: '#FFF'
    },

}) 

const Login = ({loginIsValid,loginIn, loginOut,  loading}) => {

    const [user, setUser] = useState("")
    const [pass, setPass] = useState("")

    const insets = useSafeAreaInsets()
    AntDesignIcon.loadFont()

    return (
        <KeyboardAwareScrollView>
            <Spinner 
                visible={loading}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={[styles.header, {paddingTop: insets.top}]}>
                <Text style={styles.login}>Bienvenido a la SuperApp!</Text>
            </View>
            <View style={styles.inputContainer}>
                <View style={styles.textinputContainer}>
                <TextInput
                        placeholder="Usuario"
                        value={user}
                        autoCapitalize="none"
                        onChangeText={(newUser) => setUser(newUser)}
                        style={styles.textInput}
                    />
                    <TextInput
                        placeholder="Password"
                        value={pass}
                        autoCapitalize="none"
                        onChangeText={(newPass) => setPass(newPass)}
                        style={styles.textInput}
                        secureTextEntry
                    />
                </View>
                <TouchableOpacity
                    style={styles.inputSubmit}
                    onPress={() => loginIn({user, pass})}
                >
                    <AntDesignIcon name="arrowright" color='#ecf0f1' size={60}></AntDesignIcon>
                </TouchableOpacity>

            </View>
        </KeyboardAwareScrollView>
    )
}

// PARA LOS ESTADOS
const mapStateToProps = (state) => ({
    loginIsValid: state.login.valid,
    loading: state.login.loading,
})


// PARA GATILLAR LOS ACTIONS QUE GATILLAN CAMBIOS DE ESTADO
const mapDispatchToProps = (dispatch) => ({
    loginIn: ({user,pass}) => dispatch(login({user,pass})),
})
    
    
export default connect(mapStateToProps, mapDispatchToProps)(Login);
