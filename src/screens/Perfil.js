import React, { Component, useEffect, useState } from 'react'
import { Alert, Text, View } from 'react-native'
import { Input } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'
import TituloComponent from '../components/TituloComponent'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Spinner from 'react-native-loading-spinner-overlay'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



const styles =({
    container:{
        padding: 20,
    },
    buttonContainer:{
        backgroundColor: "#27ae60",
        borderRadius: 10,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    buttonText:{
        color:"#fff",
        fontSize: 20,
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})

const Perfil = ({navigation}) => {

    const [nombre, setNombre] = useState("")
    const [correo, setCorreo] = useState("")
    const [telefono, setTelefono] = useState("")
    const [visibleSpinner, setVisibleSpinner] = useState(false)

    useEffect( () => {

        obtenerData()
        
    }, [])

    const obtenerData = async () => {
        setVisibleSpinner(true)

        const datos = await AsyncStorage.getItem("datos")
        const datosParse = datos != null ? JSON.parse(datos) : null
        
        setCorreo(datosParse != null ? datosParse.correo : null)
        setNombre(datosParse != null ? datosParse.nombre : null)
        setTelefono(datosParse != null ? datosParse.telefono : null)
        
        setVisibleSpinner(false)
    }

    const guardarDatos = async () => {

        setVisibleSpinner(true)

        const datos = JSON.stringify({"nombre": nombre, "correo": correo, "telefono":telefono})
        await AsyncStorage.setItem("datos", datos)

        setVisibleSpinner(false)

        Alert.alert("Aviso", "Tus datos fueron guardados!")        



    }



    return (
        <>
            <Spinner 
                visible={visibleSpinner}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <KeyboardAwareScrollView >

                <TituloComponent titulo="Mis datos perdonales"/>
                <View style={styles.container}>
                    <Text>Nombre</Text>
                    <Input placeholder="Ingresa tu nombre" 
                        onChangeText={(newNombre) => setNombre(newNombre)}
                        value={nombre}
                    />
                    <Text>Email</Text>
                    <Input placeholder="Ingresa tu Email" 
                        onChangeText={(newEmail) => setCorreo(newEmail)}
                        value={correo}
                        keyboardType="email-address"
                    />
                    <Text>Teefono</Text>
                    <Input placeholder="Ingresa tu Telefono" 
                        onChangeText={(newTelefono) => setTelefono(newTelefono)}
                        value={telefono}
                        defaultValue={telefono}
                        keyboardType="numeric"
                    />
                </View>
                <TouchableOpacity style={styles.buttonContainer} onPress={() => guardarDatos()}>
                    <Text style={styles.buttonText}>
                        Guardar
                    </Text>
                </TouchableOpacity>
                
                <TouchableOpacity style={styles.buttonContainer} onPress={() => obtenerData()}>
                    <Text style={styles.buttonText}>
                        Resetear
                    </Text>
                </TouchableOpacity>
            </KeyboardAwareScrollView>
        </>
    )
}

export default Perfil
